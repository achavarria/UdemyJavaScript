import { CocktailRecopesPage } from './app.po';

describe('cocktail-recopes App', function() {
  let page: CocktailRecopesPage;

  beforeEach(() => {
    page = new CocktailRecopesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('cr works!');
  });
});
