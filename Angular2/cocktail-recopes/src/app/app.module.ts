import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { RecipesComponent } from './recipes/recipes.component';
import { CocktailListComponent } from './recipes/cocktail-list/cocktail-list.component';
import { CocktailItemComponent } from './recipes/cocktail-list/cocktail-item.component';
import { CocktailsDetailsComponent } from './recipes/cocktails-details/cocktails-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipesComponent,
    CocktailListComponent,
    CocktailItemComponent,
    CocktailsDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
