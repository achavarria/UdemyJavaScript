import { Component, OnInit, Input } from '@angular/core';
import { Cocktail } from '../cocktail';

@Component({
  selector: 'cr-cocktail-item',
  templateUrl: './cocktail-item.component.html',
  styleUrls: ['./cocktail-item.component.css']
})
export class CocktailItemComponent implements OnInit {

  @Input() cocktail: Cocktail;

  constructor() { }

  ngOnInit() {
  }

}
