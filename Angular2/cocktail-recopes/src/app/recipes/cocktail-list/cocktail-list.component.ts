import { Component, OnInit } from '@angular/core';
import { Cocktail } from '../cocktail';
import { Output } from '@angular/core/src/metadata/directives';
import { EventEmitter } from '@angular/core/src/facade/async';

@Component({
  selector: 'cr-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.css']
})
export class CocktailListComponent implements OnInit {

  @Output() cocktailSelected = new EventEmitter<Cocktail>();
  cocktail = new Cocktail('Alexander','El cocktail Brandy Alexander se suele tomar después de las comidas y es ideal para los amantes de las bebidas cremosas y dulces. El combinado, en su base, es el resultado de mezclar Coñac y crema de cacao.',
  '../assets/images/alexander-cocktail.jpg');

  constructor() { }

  ngOnInit() {
  }

  onSelected(cocktail: Cocktail){
  	this.cocktailSelected.emit(cocktail);
  }

}
