import { Component, OnInit } from '@angular/core';
import { Cocktail } from '../cocktail';
import { Input } from '@angular/core/src/metadata/directives';

@Component({
  selector: 'cr-cocktails-details',
  templateUrl: './cocktails-details.component.html',
  styleUrls: ['./cocktails-details.component.css']
})
export class CocktailsDetailsComponent implements OnInit {

  @Input() cocktailSelected: Cocktail;
  
  constructor() { }

  ngOnInit() {
  }

}
