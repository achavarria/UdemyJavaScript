import { Component, OnInit } from '@angular/core';
import { Cocktail } from './cocktail';

@Component({
  selector: 'cr-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  cocktailSelected: Cocktail;

  constructor() { }

  ngOnInit() {
  }

}
