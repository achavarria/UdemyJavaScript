import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  private switch = true;
  private items = ['A','B','C','D','E','F','G'];
  private value = 75;

  ocultarMostrar(){
    this.switch = !this.switch;
  }

}
