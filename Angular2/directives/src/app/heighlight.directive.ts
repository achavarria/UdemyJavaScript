import { Directive, HostListener, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHeighlight]'
})
export class HeighlightDirective {

  @HostListener('mouseenter') mouseover(){
    this.backgroundColor = this.highlightColor;
  };

  @HostListener('mouseleave') mouseout(){
    this.backgroundColor = this.defaultColor;
  };

  @HostBinding('style.backgroundColor') get setColor(){
    return this.backgroundColor;
  }

  @Input() defaultColor = "grey";
  @Input('appHeighlight') highlightColor = "Orange";

  private backgroundColor: string;

  constructor() { //Interactuar con el elemento
    //this.elementRef.nativeElement.style.backgroundColor = "Red";
    //this.renderer.setElementStyle(this.elementRef.nativeElement, 'background-color', 'grey');
   } 

   ngOnInit(){
    this.backgroundColor = this.defaultColor;
   }

}
