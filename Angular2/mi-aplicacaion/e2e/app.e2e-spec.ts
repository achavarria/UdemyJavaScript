import { MiAplicacaionPage } from './app.po';

describe('mi-aplicacaion App', function() {
  let page: MiAplicacaionPage;

  beforeEach(() => {
    page = new MiAplicacaionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
