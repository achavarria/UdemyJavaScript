import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
 /* templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']*/
  template: `
    <h1>
        {{title}}
    </h1>
    <ma-lifecycle *ngIf = "!delete"></ma-lifecycle>
    <button (click)="delete = true">OnDestroy</button>
  `
})
export class AppComponent {
  delete = false;
  title = 'Holas mundo!';
}
