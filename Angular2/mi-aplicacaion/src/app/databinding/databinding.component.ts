import { Component } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.css']
})
export class DatabindingComponent{
  StrInter = "Esto es una interpolación de cadena con String Interpolation";
  NumInter = 5;

  activeBorder(){
    return true;
  }

onClicked(value: string){
  console.log(value);
}

}
