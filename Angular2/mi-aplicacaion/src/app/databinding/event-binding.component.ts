import { Component } from '@angular/core';
import { EventEmitter } from '@angular/common/src/facade/async';
import { Output } from '@angular/core/src/metadata/directives';

@Component({
  selector: 'ma-event-binding',
  template: `
    <button (click)="onClicked()">Prueba</button>
  `,
  styles: []
})
export class EventBindingComponent {

@Output('maClicked') clicked = new EventEmitter<string>();

  onClicked(){
    this.clicked.emit("Funciona!!");
  }
}
