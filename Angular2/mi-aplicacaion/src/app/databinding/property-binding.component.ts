import { Component, Input } from '@angular/core';

@Component({
  selector: 'ma-property-binding',
  template: `
   {{resultado}}
  `,
  styles: []
})
export class PropertyBindingComponent  {
@Input() resultado: number = 0;
}
