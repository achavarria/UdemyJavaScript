import { Component } from '@angular/core';

@Component({
  selector: 'ma-tow-way-binding',
  template: `
    <input type="text" [(ngModel)]= "persona.nombre" />
    <input type="text" [(ngModel)]= "persona.nombre" />
    <br />
    <input type="text" [(ngModel)]= "persona.edad" />
  `,
  styles: []
})
export class TowWayBindingComponent {
  persona = {
    nombre: "Alexander",
    edad: 29
  }
}
