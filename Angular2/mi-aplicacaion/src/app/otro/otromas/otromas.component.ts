import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-otromas',
  template: 
    `<article>
      <ng-content></ng-content>
    </article>`,
  styles: [`
    article {
      padding: 5px;
      border: 1px solid grey;
      boder-radius: 3px;
      margin-bottim: 10px;
    }
  `]
})
export class OtromasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
