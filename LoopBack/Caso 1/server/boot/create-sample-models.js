module.exports = function(app) {
    app.dataSources.mysqlDs.automigrate('person', function(err) {
      if (err) throw err;
  
      app.models.person.create([{
        firstname: 'Bel Cafe',
        lastname: 'Vancouver'
      }, {
        firstname: 'Three Bees Coffee House',
        lastname: 'San Mateo'
      }, {
        firstname: 'Caffe Artigiano',
        lastname: 'Vancouver'
      }, ], function(err, persons) {
        if (err) throw err;
  
        console.log('Models created: \n', persons);
      });
    });
  };